//Command design pattern.
package com.mycompany.cpit252project;

public class BuyCrypto implements Order {

    private cryptocurrency abcStock;

    public BuyCrypto(cryptocurrency abcStock) {
        this.abcStock = abcStock;
    }

    public void execute() {
        abcStock.buy();
    }
}
